<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MethodDetailStatus;
use App\Http\Resources\MethodDetailStatusResource;

class MethodDetailStatusController extends Controller
{
    public function index() 
    {
        try {
            $response = MethodDetailStatus::orderBy('id', 'ASC')->get();

            return $this->successResponse('Berhasil mendapatkan data', MethodDetailStatusResource::collection($response), 200);
        } catch (\Throwable $th) {
            return $this->errorResponse('Gagal mendapatkan data status', 500);
        }
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class MethodDetailUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'start_date' => 'required',
            'end_date' => 'required',
            'method_id' => 'required',
            'status' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Silakan isi nama terlebih dahulu.',
            'start_date.required' => 'Silakan isi tanggal mulai terlebih dahulu.',
            'end_date.required' => 'Silakan isi tanggal selesai terlebih dahulu',
            'method_id.required' => 'Silakan pilih metode terlebih dahulu',
            'status.required' => 'Silakan pilih status terlebih dahulu',
            'name.min' => 'Nama minimal terdiri dari 3 huruf',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'meta' => [
                'code' => 422, 
                'status' => 'error', 
                'message' => $validator->errors()->first()
            ], 
            'data' => []
        ], 422));
    }
}

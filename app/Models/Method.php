<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\MethodDetail;

class Method extends Model
{
    use SoftDeletes;

    protected $table = 'method';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * Get the method detail that owns the method.
     */
    public function methodDetail()
    {
        return $this->hasMany(MethodDetail::class);
    }
}
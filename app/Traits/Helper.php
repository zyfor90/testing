<?php

namespace App\Traits;

trait Helper
{
    protected function errorResponse($message, $code, $data = null)
    {
        return response()->json([
            'meta' => [
                'code' => $code, 
                'status' => 'error', 
                'message' => $message
            ], 
            'data' => $data
        ], $code);
    }

    protected function successResponse($message, $data, $code)
    {
        return response()->json([
            'meta' => [
                'code' => $code, 
                'status' => 'success', 
                'message' => $message
            ], 
            'data' => $data
        ], $code);
    }

    protected function getMonth($currentMonth) {
        switch ($currentMonth) {
            case '01':
                $month = 'januari';
                break;
            case '02':
                $month = 'februari';
                break;
            case '03':
                $month = 'maret';
                break;
            case '04':
                $month = 'april';
                break;
            case '05':
                $month = 'mei';
                break;
            case '06':
                $month = 'juni';
                break;
            case '07':
                $month = 'juli';
                break;
            case '08':
                $month = 'agustus';
                break;
            case '09':
                $month = 'september';
                break;
            case '10':
                $month = 'oktober';
                break;
            case '11':
                $month = 'november';
                break;
            case '12':
                $month = 'desember';
                break;
        }

        return $month;
    }
} 
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMethodDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('method_detail', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('method_id');
            $table->string('name');
            $table->integer('status');
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('method_id')->references('id')->on('method')->onDelete('cascade');
            $table->foreign('status')->references('id')->on('method_detail_status')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('method_detail');
    }
}

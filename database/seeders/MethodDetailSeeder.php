<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MethodDetail;

class MethodDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MethodDetail::insert([
            [
                'method_id' => 1,
                'name' => 'Fundamental of Superintendence',
                'start_date' => '2019-01-02',
                'end_date' => '2019-01-05',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Introduction to TIC Indrustry',
                'start_date' => '2019-01-03',
                'end_date' => '2019-01-05',
                'status' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Rindam "Bela Negara"',
                'start_date' => '2019-01-04',
                'end_date' => '2019-01-05',
                'status' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Human Resource Generalis',
                'start_date' => '2019-01-05',
                'end_date' => '2019-01-10',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Basic Finance for business',
                'start_date' => '2019-01-10',
                'end_date' => '2019-01-15',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Basic Auditing',
                'start_date' => '2019-02-02',
                'end_date' => '2019-02-06',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Business Legal',
                'start_date' => '2019-02-03',
                'end_date' => '2019-02-06',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'General affair ',
                'start_date' => '2019-02-04',
                'end_date' => '2019-02-06',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Risk Management',
                'start_date' => '2019-02-06',
                'end_date' => '2019-02-06',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Basic Business Process',
                'start_date' => '2019-02-12',
                'end_date' => '2019-02-15',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Basic Salesmanship',
                'start_date' => '2019-06-02',
                'end_date' => '2019-06-06',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Creative Thinking',
                'start_date' => '2019-06-02',
                'end_date' => '2019-06-06',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Data Analytics',
                'start_date' => '2019-06-02',
                'end_date' => '2019-06-06',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Managing Self Motivation',
                'start_date' => '2019-06-02',
                'end_date' => '2019-06-06',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Problem Solving & Desicion Making',
                'start_date' => '2019-06-02',
                'end_date' => '2019-06-06',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 1,
                'name' => 'Managing Performance',
                'start_date' => '2019-06-02',
                'end_date' => '2019-06-06',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 2,
                'name' => 'Sharing Practice',
                'start_date' => '2019-03-12',
                'end_date' => '2019-03-15',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 2,
                'name' => 'Sharing Practice',
                'start_date' => '2019-05-12',
                'end_date' => '2019-05-15',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 3,
                'name' => 'Ask The Expert',
                'start_date' => '2019-03-02',
                'end_date' => '2019-03-05',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 3,
                'name' => 'Ask The Expert',
                'start_date' => '2019-04-12',
                'end_date' => '2019-04-15',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 3,
                'name' => 'Ask The Expert',
                'start_date' => '2019-05-02',
                'end_date' => '2019-05-05',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 4,
                'name' => 'Group Coaching',
                'start_date' => '2019-05-12',
                'end_date' => '2019-05-15',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 5,
                'name' => 'Mentoring Session',
                'start_date' => '2019-03-05',
                'end_date' => '2019-03-10',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 5,
                'name' => 'Mentoring Session',
                'start_date' => '2019-04-12',
                'end_date' => '2019-04-15',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'method_id' => 5,
                'name' => 'Mentoring Session',
                'start_date' => '2019-05-02',
                'end_date' => '2019-05-15',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            
        ]);
    }
}

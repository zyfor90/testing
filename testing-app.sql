-- Adminer 4.8.1 PostgreSQL 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1) dump

DROP TABLE IF EXISTS "method";
DROP SEQUENCE IF EXISTS method_id_seq1;
CREATE SEQUENCE method_id_seq1 INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."method" (
    "id" bigint DEFAULT nextval('method_id_seq1') NOT NULL,
    "name" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "deleted_at" timestamp(0),
    CONSTRAINT "method_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "method" ("id", "name", "created_at", "updated_at", "deleted_at") VALUES
(1,	'Workshop/ Self Learning',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(2,	'Sharing Practice/Professional''s Talk',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(3,	'Discussion Room',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(4,	'Coaching',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(5,	'Mentoring',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL);

SELECT setval('method_id_seq1', 5, true);

DROP TABLE IF EXISTS "method_detail";
DROP SEQUENCE IF EXISTS method_detail_id_seq1;
CREATE SEQUENCE method_detail_id_seq1 INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."method_detail" (
    "id" bigint DEFAULT nextval('method_detail_id_seq1') NOT NULL,
    "method_id" bigint NOT NULL,
    "name" character varying(255) NOT NULL,
    "status" integer NOT NULL,
    "start_date" date NOT NULL,
    "end_date" date NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "deleted_at" timestamp(0),
    CONSTRAINT "method_detail_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "method_detail" ("id", "method_id", "name", "status", "start_date", "end_date", "created_at", "updated_at", "deleted_at") VALUES
(1,	1,	'Fundamental of Superintendence',	1,	'2019-01-02',	'2019-01-05',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(2,	1,	'Introduction to TIC Indrustry',	2,	'2019-01-03',	'2019-01-05',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(3,	1,	'Rindam "Bela Negara"',	3,	'2019-01-04',	'2019-01-05',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(4,	1,	'Human Resource Generalis',	1,	'2019-01-05',	'2019-01-10',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(5,	1,	'Basic Finance for business',	1,	'2019-01-10',	'2019-01-15',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(6,	1,	'Basic Auditing',	1,	'2019-02-02',	'2019-02-06',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(7,	1,	'Business Legal',	1,	'2019-02-03',	'2019-02-06',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(8,	1,	'General affair ',	1,	'2019-02-04',	'2019-02-06',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(9,	1,	'Risk Management',	1,	'2019-02-06',	'2019-02-06',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(10,	1,	'Basic Business Process',	1,	'2019-02-12',	'2019-02-15',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(11,	1,	'Basic Salesmanship',	1,	'2019-06-02',	'2019-06-06',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(12,	1,	'Creative Thinking',	1,	'2019-06-02',	'2019-06-06',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(13,	1,	'Data Analytics',	1,	'2019-06-02',	'2019-06-06',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(14,	1,	'Managing Self Motivation',	1,	'2019-06-02',	'2019-06-06',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(15,	1,	'Problem Solving & Desicion Making',	1,	'2019-06-02',	'2019-06-06',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(16,	1,	'Managing Performance',	1,	'2019-06-02',	'2019-06-06',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(17,	2,	'Sharing Practice',	1,	'2019-03-12',	'2019-03-15',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(18,	2,	'Sharing Practice',	1,	'2019-05-12',	'2019-05-15',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(19,	3,	'Ask The Expert',	1,	'2019-03-02',	'2019-03-05',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(20,	3,	'Ask The Expert',	1,	'2019-04-12',	'2019-04-15',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(21,	3,	'Ask The Expert',	1,	'2019-05-02',	'2019-05-05',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(22,	4,	'Group Coaching',	1,	'2019-05-12',	'2019-05-15',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(23,	5,	'Mentoring Session',	1,	'2019-03-05',	'2019-03-10',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(24,	5,	'Mentoring Session',	1,	'2019-04-12',	'2019-04-15',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(25,	5,	'Mentoring Session',	1,	'2019-05-02',	'2019-05-15',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL);

SELECT setval('method_detail_id_seq1', 25, true);

DROP TABLE IF EXISTS "method_detail_status";
DROP SEQUENCE IF EXISTS method_detail_status_id_seq1;
CREATE SEQUENCE method_detail_status_id_seq1 INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."method_detail_status" (
    "id" bigint DEFAULT nextval('method_detail_status_id_seq1') NOT NULL,
    "name" character varying(255) NOT NULL,
    "color" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "deleted_at" timestamp(0),
    CONSTRAINT "method_detail_status_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "method_detail_status" ("id", "name", "color", "created_at", "updated_at", "deleted_at") VALUES
(1,	'Berlangsung',	'badge-primary',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(2,	'Selesai',	'badge-success',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL),
(3,	'Akan Datang',	'badge-danger',	'2023-01-01 15:31:48',	'2023-01-01 15:31:48',	NULL);

SELECT setval('method_detail_status_id_seq1', 3, true);

ALTER TABLE ONLY "public"."method_detail" ADD CONSTRAINT "method_detail_method_id_foreign" FOREIGN KEY (method_id) REFERENCES method(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."method_detail" ADD CONSTRAINT "method_detail_status_foreign" FOREIGN KEY (status) REFERENCES method_detail_status(id) ON DELETE CASCADE NOT DEFERRABLE;

-- 2023-01-01 22:34:18.584001+07

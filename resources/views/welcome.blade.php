@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-header">
        <span class="float-left m-2">Laporan</span>
        <div class="float-right">
            <button data-toggle="modal" data-target="#ModalCreateMethod" type="button" name="button" class="m-2 btn btn-sm btn-primary" id="ButtonCreateMethod">Buat Metode Baru</button>
            <button data-toggle="modal" data-target="#ModalCreateMethodDetail" type="button" name="button" class="m-2 btn btn-sm btn-primary" id="ButtonCreateMethod">Buat Detail Metode Baru</button>
        </div>    
    </div>
    
    <div class="card-body">
        <div class="co-lg-12">
            <small class="text-danger">
                * Tekan tulisan dibawah untuk mengedit dan menghapus data.
            </small>
            <hr>
            <label for="name"><small>Lihat Data Per Tahun :</small> </label> 
            <select name="status" class="form-control" id="SelectYear">
                <option value="">Lihat Semua</option>
            </select>
            <br>
        </div>
        <div class="table-responsive">
            <table id="table-main" class="table table-bordered" style="width: 600%; max-width: 400%;">
                <tr>
                    <th width="350px">Metode</th>
                    <th width="350px">Januari <span class ='PutYear'></span></th>
                    <th width="350px">Februari <span class ='PutYear'></span></th>
                    <th width="350px">Maret <span class ='PutYear'></span></th>
                    <th width="350px">April <span class ='PutYear'></span></th>
                    <th width="350px">Mei <span class ='PutYear'></span></th>
                    <th width="350px">Juni <span class ='PutYear'></span></th>
                    <th width="350px">Juli <span class ='PutYear'></span></th>
                    <th width="350px">Agustus <span class ='PutYear'></span></th>
                    <th width="350px">September <span class ='PutYear'></span></th>
                    <th width="350px">Oktober <span class ='PutYear'></span></th>
                    <th width="350px">November <span class ='PutYear'></span></th>
                    <th width="350px">Desember <span class ='PutYear'></span></th>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalCreateMethod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form class="" action="#" method="post" id="FormCreateMethod">
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <span class="font-weight-bold">Buat Metode Baru</span>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="hidden" name="id" value="" id="id" readonly>
                                        <label for="name" class="font-weight-bold">Nama Metode</label> <span class="text-danger">*</span>
                                        <input type="text" class="form-control" name="name" placeholder="Nama metode" value="" id="NameCreateMethod">
                                    </div>
                                </div>
                                <div class="col-lg-12 text-right mt-3">
                                    <button type="button" class="btn btn-light border" data-dismiss="modal">Batalkan</button>
                                    <button class="btn btn-light border" id="ConfirmCreateButton">Konfirmasi</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalUpdateMethod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="card">
                    <div class="card-header">
                        <span class="font-weight-bold">Edit Metode</span>
                    </div>
                    <div class="card-body">
                        <form class="" action="#" method="post" id="FormUpdateMethod">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="hidden" name="id" value="" id="IDUpdateMethod" readonly>
                                        <label for="name" class="font-weight-bold">Nama Metode</label> <span class="text-danger">*</span>
                                        <input type="text" class="form-control" name="name" placeholder="Nama metode" value="" id="NameUpdateMethod">
                                    </div>
                                </div>
                                <div class="col-lg-12 text-right">
                                    <button type="button" class="btn btn-light border" data-dismiss="modal">Batalkan</button>
                                    <button class="btn btn-light border" id="ConfirmUpdateButton">Konfirmasi</button>
                                </div>
                            </div>
                        </form>
                    </div> 
                </div>
                <div class="row mt-2">
                    <div class="col-lg-12">
                        <span style="font-size:10px;" class="mt-1 text-danger">* Dengan menekan tombol hapus, maka kamu akan langsung menghapus data ini, apakah kamu yakin ?</span>
                    </div>
                    <div class="col-lg-12 text-center">
                        <form class="" action="#" method="post" id="FormDeleteMethod">
                                {{ csrf_field() }}
                            <input type="hidden" name="id" value="" id="IDDeleteMethod" readonly>
                            <button class="btn btn-sm btn-block bg-danger text-white btn-light border" id="ConfirmDeleteButton">Ya, Hapus Data!</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalCreateMethodDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form class="" action="#" method="post" id="FormCreateMethodDetail">
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <span class="font-weight-bold">Buat Kegiatan Baru</span>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="method_id" class="font-weight-bold">Pilih Metode</label> <span class="text-danger">*</span>
                                        <select name="method_id" id="MethodCreateMethodDetail" class="form-control method-selection">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="id" value="" id="id" readonly>
                                        <label for="name" class="font-weight-bold">Nama Kegiatan</label> <span class="text-danger">*</span>
                                        <input type="text" class="form-control" name="name" placeholder="Nama metode" value="" id="NameCreateMethodDetail">
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="font-weight-bold">Tanggal Mulai</label> <span class="text-danger">*</span>
                                        <input type="date" class="form-control" name="start_date" placeholder="Nama metode" value="2019-01-01" id="StartCreateMethodDetail">
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="font-weight-bold">Tanggal Selesai</label> <span class="text-danger">*</span>
                                        <input type="date" class="form-control" name="end_date" placeholder="Nama metode" value="2019-01-01" id="EndCreateMethodDetail">
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="font-weight-bold">Status Kegiatan</label> <span class="text-danger">*</span>
                                        <select name="status" id="StatusCreateMethodDetail" class="SelectStatus form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12 text-right mt-3">
                                    <button type="button" class="btn btn-light border" data-dismiss="modal">Batalkan</button>
                                    <button class="btn btn-light border" id="ConfirmCreateButtonMethodDetail">Konfirmasi</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalUpdateMethodDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="card">
                    <div class="card-header">
                        <span class="font-weight-bold">Edit Kegiatan</span>
                    </div>
                    <div class="card-body">
                        <form class="" action="#" method="post" id="FormUpdateMethodDetail">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="method_id" class="font-weight-bold">Pilih Metode</label> <span class="text-danger">*</span>
                                        <select name="method_id" class="form-control method-selection" id="MethodUpdateMethodDetail">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="id" value="" id="IDUpdateMethodDetail" readonly>
                                        <label for="name" class="font-weight-bold">Nama Kegiatan</label> <span class="text-danger">*</span>
                                        <input type="text" class="form-control" name="name" placeholder="Nama metode" value="" id="NameUpdateMethodDetail">
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="font-weight-bold">Tanggal Mulai</label> <span class="text-danger">*</span>
                                        <input type="date" class="form-control" name="start_date" placeholder="Nama metode" value="2019-01-01" id="StartUpdateMethodDetail">
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="font-weight-bold">Tanggal Selesai</label> <span class="text-danger">*</span>
                                        <input type="date" class="form-control" name="end_date" placeholder="Nama metode" value="2019-01-01" id="EndUpdateMethodDetail">
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="font-weight-bold">Status Kegiatan</label> <span class="text-danger">*</span>
                                        <select name="status" id="StatusUpdateMethodDetail" class="SelectStatus form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12 text-right mt-3">
                                    <button type="button" class="btn btn-light border" data-dismiss="modal">Batalkan</button>
                                    <button class="btn btn-light border" id="ConfirmUpdateButtonMethodDetail">Konfirmasi</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-lg-12">
                        <span style="font-size:10px;" class="mt-1 text-danger">* Dengan menekan tombol hapus, maka kamu akan langsung menghapus data ini, apakah kamu yakin ?</span>
                    </div>
                    <div class="col-lg-12 text-center">
                        <form class="" action="#" method="post" id="FormDeleteMethodDetail">
                                {{ csrf_field() }}
                            <input type="hidden" name="id" value="" id="IDDeleteMethodDetail" readonly>
                            <button class="btn btn-sm btn-block bg-danger text-white btn-light border" id="ConfirmDeleteButtonMethodDetail">Ya, Hapus Data!</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('assets/js/helper/methodHelper.js') }}" charset="utf-8"></script>
@endpush
